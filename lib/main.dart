import 'dart:async';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Color _color;

  StreamController<Color> _colorConroller;

  StreamController<int> _redConroller;
  StreamController<int> _greenConroller;
  StreamController<int> _blueConroller;

  StreamSubscription _redSubscription;
  StreamSubscription _greenSubscription;
  StreamSubscription _blueSubscription;

  double _r = 0;
  double _g = 0;
  double _b = 0;

  @override
  void initState() {
    super.initState();

    _color = Colors.black;

    _colorConroller = StreamController<Color>();

    _redConroller = StreamController<int>();
    _greenConroller = StreamController<int>();
    _blueConroller = StreamController<int>();

    _redSubscription = _redConroller.stream.listen((event) => _colorConroller.add(_color = _color.withRed(event)));
    _greenSubscription = _greenConroller.stream.listen((event) => _colorConroller.add(_color = _color.withGreen(event)));
    _blueSubscription = _blueConroller.stream.listen((event) => _colorConroller.add(_color = _color.withBlue(event)));
  }

  @override
  void dispose() {
    _colorConroller.close();

    _redSubscription.cancel();
    _greenSubscription.cancel();
    _blueSubscription.cancel();

    _redConroller.close();
    _greenConroller.close();
    _blueConroller.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: StreamBuilder<Color>(
          stream: _colorConroller.stream,
          initialData: _color,
          builder: (context, snapshot) {
            return ColoredBox(
              color: snapshot.data,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Slider(
                    onChanged: (value) => _redConroller.add((_r = value).toInt()),
                    min: 0,
                    max: 255,
                    value: _r,
                    activeColor: Colors.red,
                  ),
                  Slider(
                    onChanged: (value) => _greenConroller.add((_g = value).toInt()),
                    min: 0,
                    max: 255,
                    value: _g,
                    activeColor: Colors.green,
                  ),
                  Slider(
                    onChanged: (value) => _blueConroller.add((_b = value).toInt()),
                    min: 0,
                    max: 255,
                    value: _b,
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
